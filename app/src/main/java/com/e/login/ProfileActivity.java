package com.e.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import android.net.Uri;
import com.facebook.GraphResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.LoginStatusCallback;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GetUserCallback.IGetUserResponse {
 private ImageView profileiimg;
 private TextView name;
 private TextView email;
 private TextView id;
 private Button singout;
 private GoogleApiClient googleApiClient;
 private GoogleSignInOptions gso;
    private boolean isLoggedIn=false;
    private String isLoggedBy="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        profileiimg=findViewById(R.id.profileimg);
        id=findViewById(R.id.id);
        name=findViewById(R.id.name);
        email=findViewById(R.id.id);
        singout=findViewById(R.id.singout);
        gso= new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        googleApiClient= new GoogleApiClient.Builder(this).enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso).build();

        singout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLoggedIn) {
                    switch(isLoggedBy) {
                        case "Google":
                            Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback(new ResultCallback<Status>() {
                                @Override
                                public void onResult(@NonNull Status status) {
                                    if(status.isSuccess()) {
                                        SharedPreferences.Editor pref =  PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                                        pref.putBoolean("isLoggedIn",false);
                                        pref.commit();
                                        gotoMainActivity();
                                    }
                                    else
                                        Toast.makeText(ProfileActivity.this,"log Out failed",Toast.LENGTH_LONG).show();
                                }
                            });
                            break;
                        case "Facebook":
                            SharedPreferences.Editor pref =  PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                            pref.putBoolean("isLoggedIn",false);
                            pref.commit();
                            LoginManager.getInstance().logOut();
                            gotoMainActivity();
                            break;
                        default:
                            break;
                    }

                }

            }
        });
    }
    private void gotoMainActivity() {
        startActivity(new Intent(ProfileActivity.this, MainActivity.class));
        finish();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
     private void handleSignInResult(GoogleSignInResult result){
        if( result.isSuccess()){
            GoogleSignInAccount account= result.getSignInAccount();
            name.setText(account.getDisplayName());
            email.setText(account.getEmail());
            id.setText(account.getId());
            Picasso.get().load(account.getPhotoUrl()).placeholder(R.mipmap.ic_launcher).into(profileiimg);
            setIsLoggedIn("Google",true);
        }else {
            setIsLoggedIn("Google",false);
        }
     }

    @Override
    protected void onStart() {
        super.onStart();
        UserRequest.makeUserRequest(new GetUserCallback(ProfileActivity.this).getCallback());
        OptionalPendingResult<GoogleSignInResult>opr= Auth.GoogleSignInApi.silentSignIn(googleApiClient);
        if(opr.isDone()){
            GoogleSignInResult result=opr.get();
            handleSignInResult(result);
        }else{
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult result) {
                    handleSignInResult(result);
                }
            });
        }
    }

    @Override
    public void onCompleted(User user) {
        profileiimg.setImageURI(user.getPicture());
        name.setText(user.getName());
        id.setText(user.getId());
        if (user.getEmail() == null) {
            email.setText("No EMail");
            email.setTextColor(Color.RED);
        } else {
            email.setText(user.getEmail());
            email.setTextColor(Color.BLACK);
        }
        setIsLoggedIn("Facebook",true);
    }

    private void setIsLoggedIn(String loggedInBy,boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
        this.isLoggedBy = loggedInBy;
       SharedPreferences.Editor pref =  PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        pref.putString("loggedInBy",loggedInBy);
        pref.putBoolean("isLoggedIn",isLoggedIn);
        pref.commit();
    }
}

class UserRequest {
    private static final String ME_ENDPOINT = "/me";

    public static void makeUserRequest(GraphRequest.Callback callback) {
        Bundle params = new Bundle();
        params.putString("fields", "picture,name,id,email,permissions");


        GraphRequest request = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                ME_ENDPOINT,
                params,
                HttpMethod.GET,
                callback
        );
        request.executeAsync();
    }
}

class GetUserCallback {

    public interface IGetUserResponse {
        void onCompleted(User user);
    }

    private IGetUserResponse mGetUserResponse;
    private GraphRequest.Callback mCallback;

    public GetUserCallback(final IGetUserResponse getUserResponse) {

        mGetUserResponse = getUserResponse;
        mCallback = new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse response) {
                User user = null;
                try {
                    JSONObject userObj = response.getJSONObject();
                    if (userObj == null) {
                        return;
                    }
                    user = jsonToUser(userObj);

                } catch (JSONException e) {
                }
                mGetUserResponse.onCompleted(user);
            }
        };
    }

    private User jsonToUser(JSONObject user) throws JSONException {
        Uri picture = Uri.parse(user.getJSONObject("picture").getJSONObject("data").getString
                ("url"));
        String name = user.getString("name");
        String id = user.getString("id");
        String email = null;
        if (user.has("email")) {
            email = user.getString("email");
        }
        StringBuilder builder = new StringBuilder();
        JSONArray perms = user.getJSONObject("permissions").getJSONArray("data");
        builder.append("Permissions:\n");
        for (int i = 0; i < perms.length(); i++) {
            builder.append(perms.getJSONObject(i).get("permission")).append(": ").append(perms
                    .getJSONObject(i).get("status")).append("\n");
        }
        String permissions = builder.toString();

        return new User(picture, name, id, email, permissions);
    }

    public GraphRequest.Callback getCallback() {
        return mCallback;
    }
}

class User {
    private final Uri picture;
    private final String name;
    private final String id;
    private final String email;
    private final String permissions;

    public User(Uri picture, String name,
                String id, String email, String permissions) {
        this.picture = picture;
        this.name = name;
        this.id = id;
        this.email = email;
        this.permissions = permissions;
    }

    public Uri getPicture() {
        return picture;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPermissions() {
        return permissions;
    }
}
